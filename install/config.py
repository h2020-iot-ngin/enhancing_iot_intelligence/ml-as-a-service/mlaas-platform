# Disclaimer
# ----------
#
# Copyright (C) 2023 Capgemini
#
#
# This file is part of IoT-NGIN Machine Learning as a Service framework.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/usr/bin/env python
"""Configuration command-line utility for MLaaS platform installation."""

import re
import os
import shutil
import stat
import argparse
from pathlib import Path

#Load the parameters from configuration file
def load_params(configfile):
    """Load the parameters from configuration file"""
    params = {}

    try:
        with open(configfile, 'r') as fp:
            lines = fp.readlines()
            for line in lines:
                line = line.strip()
                matchparam = re.match(r'(\S*).*=\s*(.*)',line)
                if matchparam:
                    kparam = matchparam.group(1)
                    vparam = matchparam.group(2)
                    (kparam1,kparam2) = kparam.split("_")
                    kparam = "{{" + kparam1 + "."+ kparam2 + "}}"
                    params[kparam] = vparam
    except Exception as e:
        print("Unable to open config file", configfile,":",e)

    return params


parser = argparse.ArgumentParser(prog="mlaas-install",
    description="Install IoTNGIN MLaaS platform",
    epilog="Thanks for using %(prog)s!",
)

dest = parser.add_argument_group("destination directory")
dest.add_argument("path")
parser.add_argument("--version", action="version", version="%(prog)s 0.1.0")
config = parser.add_argument_group("configuration file")
config.add_argument("-c", "--config",default="config.txt")
source = parser.add_argument_group("source directory")
source.add_argument("-s", "--source",default=Path.cwd())
args = parser.parse_args()
target_dir = Path(args.path)
print (target_dir)
print (args.config)
print (args.source)

# load parameters
if args.config is None:
    configfile = "config.txt"
else:
    configfile = args.config
print("Load parameters from",configfile)
params = load_params(configfile)
print (params)

if os.path.isdir(target_dir):
    print ("Directory already exits - please delete",target_dir)
    exit()

if os.path.isdir(args.source):
    print ('Copy file from', args.source,'to',target_dir)
    try:
        shutil.copytree(args.source , target_dir)
    except Exception as e:
        print('Unable to copy from', args.source,'to',target_dir,e)

exts = [".yaml", ".txt"]
myfiles = [p for p in target_dir.rglob('*') if p.suffix in exts]

for path in sorted(myfiles):
    with open(path, 'r') as fp:
    # read all content of a file and search string
        found = False
        printfile = False
        lines = fp.read()
        for key in params:
            if key in lines:
                lines = lines.replace(key,params[key])
                found = True
                    
    if found:
        if printfile:
            print ("writing file",path)
            
        path.write_text(lines)




    
