# Machine as a Service (MLaaS) Platform
Refer to the European Commission project [IoTNGIN]( https://iot-ngin.eu/) deliverables for further information about the platform's architecture and capabilities.
The Machine Learning Platform framework offers the functionalities required to perform Machine Learning activities such as data preparation, model training, ML pipeline, experiment and inference service.
## Prerequisites
The installation of the platform depends on the availability of:
- Kubernetes v1.21 cluster
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- A default [storage class](https://kubernetes.io/docs/concepts/storage/storage-classes/)
- A mechanism to get two load-balancing ip addresses (such as for example [metallb]( https://metallb.org/)
- [Nginx ingress]( https://kubernetes.github.io/ingress-nginx/)
- Cert-manager
- DNS entries for the hostnames used in the config file

## Main Platform Components
The platform consists of the main following components:
- [Kubeflow]( https://www.kubeflow.org/)
- [Kserve](https://kserve.github.io/website/)
- [Minio]( https://min.io/)
- [Camel-k](https://camel.apache.org/camel-k/1.12.x/index.html)
- [Postgres](https://www.postgresql.org)
- [ArgoCD](https://argo-cd.readthedocs.io/)
- [Istio]( https://istio.io/)
- [Nginx ingress configuration]( https://kubernetes.github.io/ingress-nginx/)

Kubeflow version is v1.5 from [Kubeflow Manifest]( https://github.com/kubeflow/manifests) kubeflow-manifests-6b60781 dated 07-Jul-2022

The platform uses LetsEncrypt (https://letsencrypt.org/) generate external certificates

## Installation overview
Installation of the platform is a three steps process:
1. Preparation work
2. Argocd installation
3. The installation of the MLaaS platform
## Preparation Work
Preparation work aiming at creating the Git repository required for the installation. Steps are:
1. Download or Clone the MLaaS files from the Git repository
2. Create the installation files from a config file
3. Upload the git files to the git repository configured in the config file
### Download MLaaS Git repository
Download or clone the [MLaaS Git repository]( https://gitlab.com/h2020-iot-ngin/enhancing_iot_intelligence/ml-as-a-service/mlaas-platform) in a local directory
### Create installation files
A simple python [config script](./install/config.py) and accompanying [config file]( ./install/config.txt) is use to configure the files with the values to be used for the installation such as the domain names, username/password, etc.
1. Go to your local repository
2. Edit the [config file]( ./install/config.txt) and set the values related to your installation
3. Run the config.py script in the [install](./install) directory to create the git files for the installation. For example (on Windows) to create the git files in the c:\temp\mlaasfrom the template files in ..\platform using the parameters in the local config.txt files :
```bash
C:\ML-as-a-service\install>config.py -c config.txt -s ..\platform c:\temp\mlaas
```
### Upload files to Git repository
Upload the created repository with the installation file to the Git repository specified in the git_url parameter of the config file
## Argocd
### Argocd installation
- Go to your local git repository:
```bash
cd <repository>
```

- Install and configure [ArgoCD](https://argo-cd.readthedocs.io/en/stable/):
```bash
cd .\mlaas\tools\argocd\base
kubectl apply -k .
```

- To get the ArgoCD password:
```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" 
```
and then decode with base64  library or online decoder (for example [Base64 Decode and Encode – Online](https://www.base64decode.org/))

### Argocd configuration
ArgoCD must be configured to connect to the Git repository and create some projects 
- Connect to ArgoCD
- In settings/Repositories, connect your git repository
- In setting/Projects create the kubeflow, apps and tools projects (using * everywhere is fine) 

## MLaaS Platform Installation
Installation is configuring the cluster with the resources of the MLaaS platform mainly using ArgoCD applications files. Steps are:
1. Installation of the tools
2. Installation of Kubeflow
3. Installation of the additional apps
2. Installation of Camel-k and MinIO
### Installation of the tools
Tools are install using ArgoCD applications. There are located in [tools argocd-apps]( ./platform/mlaas/tools/argocd-apps) repository. To install:
- go to platform/mlaas/tools/argocd-apps
- install applications: for all yaml files run command:
```bash
kubectl -n argocd -f <yaml-file>
```
Tools applications should appear in the ArgoCD dashboard.

### Installation of Kubeflow
Kubelow is installed using ArgoCD applications. There are located in [tools argocd-apps]( ./platform/mlaas/kubeflow/argocd-apps) repository. To install:
- go to platform/mlaas/kubeflow/argocd-apps
- install applications: for all yaml files run command:
```bash
kubectl -n argocd -f <yaml-file>
```
### Installation of additional applications
Kubelow is installed using ArgoCD applications. There are located in [tools argocd-apps]( ./platform/mlaas/apps/argocd-apps) repository. To install:
- go to platform/mlaas/apps/argocd-apps
- install applications: for all yaml files run command:
```bash
kubectl -n argocd -f <yaml-file>
```
Kubeflow applications should appear in the ArgoCD dashboard. It may take a while for all resources to synchronize.

### Camel-k
Installation of Camel-k requires to first create a secret and then the ArgoCD application. Required files are in [Camel-k]( ./mlaas/apps/camel-k) repository. To install:
- go to “mlaas/apps/camel-k” repository
- create secret by running the command in file secret.txt
- Install camel-k:
```bash
kubectl apply -n argocd -f camel-k.yaml
```
Camel-k application should appear in the ArgoCD dashboard

### Minio
To install Minio:
- go to “mlaas/apps/minio” repository
- Install minio with the following commands:
```bash
kubectl apply -f minio.yaml
Kubectl apply -f minio-ingress.yaml
```
Minio application should appear in the ArgoCD dashboard

# Accessing ArgoCD Dashboard

By default the ArgoCD dashboard (UI) is installed with a Nodeport configuration: 30000 for HTTP and 30001 for HTTPS. Access should then be possible via http://<node-ip>:30000 or http://<node-ip>:30001

It can be accessed also with port forwarding, for example:

```bash
kubectl port-forward svc/argocd-server -n argocd 8080:80
```

The UI will now be accessible at `localhost:8080` and can be accessed with the initial admin password. The password is stored in a secret and can be read as follows:

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

If you wish to update the password, this can be done using the [argcd cli](https://github.com/argoproj/argo-cd/releases/latest), using the following commands:
```bash
argocd login localhost:8080
argocd account update-password
```
# PgAdmin
PgAdmin is installed as part of the tools, however without nodeport nor ingress. It can be accessed using port forwarding, for example:

```bash
kubectl port-forward svc/pgadmin-svc-n postgres 8080:80
```

The UI will now be accessible at `localhost:8080` and can be accessed with the configured admin user and password.


