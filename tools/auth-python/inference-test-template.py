# Disclaimer
# ----------
#
# Copyright (C) 2023 Capgemini
#
#
# This file is part of IoT-NGIN Machine Learning as a Service framework.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# Exemple of a script generating authservice_session cookie for kubeflow authentication
# an then use cookie to make a prediction on a sklearn-iris inference service
#
# To use script, change:
# <kubeflow-url> : URL of kubeflow - ex: https://kubeflow.example.com
# <infsrv-url>: URL of the kserve inferrence service used to test - ex: http://sklearn-iris.testuser.kserve.example.com/v1/models/sklearn-iris:predict
# <username>/<pasword>: username and password of a local DEX user - ex: testuser/ pass123
# <kcusername>/<kcpasword>: username and password of a Keycloak user - ex: testuser/ pass123
#
#!/usr/bin/env python
"""command-line utility to generate Kubeflow token for MLaaS platform authentication"""
import requests
import re
import json
import mlaas_tools

# URL of kubeflow instance
BASEURL = '<kubeflow-url>'

# URL for the inference service
URLSRV = '<infsrv-url>'

# username / password for DEX local authentication
# Replace <username> and <password> with your values
USER = '<username>'
PASSW = '<pasword>'

# username / password for keycloak authentication
# Replace <username> and <password> with your values
USERKC = '<kcusername>'
PASSWKC = '<kcpasword>'

#
# get authservice_session from user in dex
#
(authservice_session, status, err) = mlaas_tools.auth_local(BASEURL, USER, PASSW)
if status != 0:
    print ("Can't get authservice_session:\n=>", "status: ",status, "\nerr: ",err)
    exit()

print ("authservice_session local=",authservice_session)

#
# get authservice_session from user in Keycloak
#
(authservice_sessionkc, status, err) = mlaas_tools.auth_oidc(BASEURL, USERKC, PASSWKC)
if status != 0:
    print ("Can't get authservice_session:\n=>", "status: ",status, "\nerr: ",err)
    exit()

print ("authservice_session keycloak=",authservice_sessionkc)

#
# Make prediction using dex session and http
#
print ("\nRun prediction local auth",URLSRV)
headers = {'Content-Type': 'application/x-www-form-urlencoded','Cookie': 'authservice_session='+authservice_session}
data =  {'instances': [[6.8,  2.8,  4.8,  1.4], [6.0,  3.4,  4.5,  1.6]]}

r = requests.post(URLSRV, headers=headers, data=json.dumps(data), allow_redirects=True)
#print(r.status_code)

# check status is OK (200)
if not re.search(r'(.*)200\s*(.*)',str(r.status_code),re.I):
    print("=> expected code 200, got ", str(r.status_code))
else:
    print("Prediction with local auth:", str(r.text))

#
# Make prediction using keycloak session and https
#
print ("\nRun prediction keycloak auth",URLSRV)
headers = {'Content-Type': 'application/x-www-form-urlencoded','Cookie': 'authservice_session='+authservice_sessionkc}
data =  {'instances': [[6.8,  2.8,  4.8,  1.4], [6.0,  3.4,  4.5,  1.6]]}

r = requests.post(URLSRV, headers=headers, data=json.dumps(data), verify=False, allow_redirects=False)
#print(r.status_code)

# check status is OK (200)
if not re.search(r'(.*)200\s*(.*)',str(r.status_code),re.I):
    print("=> expected code 200, got ", str(r.status_code))
else:
    print("Prediction with keycloak auth:", str(r.text))

