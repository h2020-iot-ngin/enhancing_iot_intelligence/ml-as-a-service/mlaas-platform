# Disclaimer
# ----------
#
# Copyright (C) 2023 Capgemini
#
#
# This file is part of IoT-NGIN Machine Learning as a Service framework.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# Library which can be used to generateauthservice_session cookie for kubeflow authentication
# See exemple in script inference-test-template.py
#
# - auth_local: used to generate token from local dex user
# - auth_oidc: used to generate token from a user in Keycloak
#
#!/usr/bin/env python
"""Library utility to generate Kubeflow token for MLaaS platform authentication"""

import requests
import re

#
# get authservice_session cookie with dex local authentication
#
def auth_local(kfurl, user, passwd):

    authsession = ""
    
    # Step 1
    # get client-id and state
    #
    url = kfurl
    r = requests.get(url, allow_redirects=True)
    resencode = r.encoding
 
    # check status is OK (200)
    if not re.search(r'(.*)200\s*(.*)',str(r.status_code),re.I):
        return authsession, 1, url + "=> expected code 200, got " + str(r.status_code)

    # look for req value
    ok = False
    for line in r.iter_lines():
        line = line.decode(resencode)
        a = re.search(r'(.*)local\?req=(\w+)"s*(.*)',line,re.I)
        if a:
            b = re.search(r'.*href="(\S*)".*',line,re.I)
            if b:
                href = b.group(1)
                ok = True
                break


    if not ok: return authsession, 2, "expected req value but not found"

    # Step 2
    # Authenticate with user and pass
    #
    url = kfurl + '/dex/auth/local'
    url = kfurl + href
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {'login': user, 'password': passwd}
    r = requests.post(url, headers=headers, data=data, allow_redirects=True)

    # check status is OK (200)
    if not re.search(r'(.*)200\s*(.*)',str(r.status_code),re.I):
        return authsession, 3, url + " - expected code 200, got " + str(r.status_code)

    # extract authservice_session value from cookie
    ok = False
    for r1 in r.history:
        cookies = r1.cookies.get_dict()
        if 'authservice_session' in cookies:
            authsession = cookies['authservice_session']
            ok = True
            break

    if not ok: return authsession, 4, url + " expected authservice_session cookie - not found"

    # return authservice_session
    return authsession, 0, ""

#
# get authservice_session cookie with keycloak (oidc) authentication
#
def auth_oidc(kfurl, user, passwd):

    status = 0
    err = ""
    authsession = ""
    
    # Step 1
    # get client-id and state
    #
    url = kfurl
    r = requests.get(url, allow_redirects=True)
    resencode = r.encoding

    # check status is OK (200)
    if not re.search(r'(.*)200\s*(.*)',str(r.status_code),re.I):
        return authsession, 1, url + "\n=> expected code 200, got " + str(r.status_code)

    # look for req value
    ok = False
    for line in r.iter_lines():
        line = line.decode(resencode)
        a = re.search(r'(.*)keycloak\?req=(\w+)"s*(.*)',line,re.I)
        if a:
            b = re.search(r'.*href="(\S*)".*',line,re.I)
            if b:
                href = b.group(1)
                ok = True
                break

    if not ok: return authsession, 2, url + "\n=> expected req value but not found"

    # Step 2
    # get parameters
    #
    url = kfurl + href
    r = requests.get(url, allow_redirects=True)
    cookies = r.cookies

    # check status is OK (200)
    if not re.search(r'(.*)200\s*(.*)',str(r.status_code),re.I):
        return authsession, 3, url + "\n=> expected code 200, got " + str(r.status_code)

    # look for action value
    ok = False
    for line in r.iter_lines():
        line = line.decode(r.encoding)
        a = re.search(r'(.*)action="(\S*)"(.*)',line,re.I)
        if a:
            newloc = a.group(2).replace("&amp;","&")          
            ok = True
            break

    if not ok: return authsession, 4, url + " - expected action value but not found"
    
    # Step 3
    # Authenticate with user and pass
    #
    url = newloc
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {'username': user, 'password': passwd, 'credentialId': ""}
    r = requests.post(url, headers=headers, data=data, cookies=cookies, allow_redirects=True)

    # check status is OK (200)
    if not re.search(r'(.*)200\s*(.*)',str(r.status_code),re.I):
        return authsession, 5, url + " - expected code 200, got " + str(r.status_code)

    # extract authservice_session value from cookie
    ok = False
    for r1 in r.history:
        cookies = r1.cookies.get_dict()
        if 'authservice_session' in cookies:
            authsession = cookies['authservice_session']
            ok = True
            break

    if not ok: return authsession, 6, url + " expected authservice_session cookie - not found"
    
    # return authservice_session
    return authsession, status, err

